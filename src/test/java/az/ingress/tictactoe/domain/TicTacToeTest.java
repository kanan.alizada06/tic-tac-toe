package az.ingress.tictactoe.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author Kanan
 */
@ExtendWith(MockitoExtension.class)
public class TicTacToeTest {

    @InjectMocks
    private TicTacToe ticTacToe;

    @Test
    void givenParametersWhenXOutsideBoardThenRuntimeException() {
        //given
        int x = 4;
        int o = 1;

        //when && then
        assertThatThrownBy(() -> ticTacToe.play(x, o))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside of the board");
    }

    @Test
    void givenParametersWhenOOutsideBoardThenRuntimeException() {
        //given
        int x = 1;
        int o = 4;

        //when && then
        assertThatThrownBy(() -> ticTacToe.play(x, o))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("O is outside of the board");
    }

    @Test
    void givenParametersWhenBoxOccupiedThenRuntimeException() {
        //given
        int x = 1;
        int o = 1;

        ticTacToe.play(x, o);

        //when && then
        assertThatThrownBy(() -> ticTacToe.play(x, o))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    void whenFirstTurnThenPlayerX() {
        //given
        char expected = 'X';

        //when
        char actual = ticTacToe.nextPlayer();

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenLastTurnXWhenNextPlayerThenPlayerO() {
        //given
        int x = 1;
        int o = 1;
        char expected = 'O';

        //when
        ticTacToe.play(x, o);
        char actual = ticTacToe.nextPlayer();

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenLastTurnOWhenNextPlayerThenPlayerX() {
        //given
        int x = 1;
        int o = 1;
        char expected = 'X';

        //when
        ticTacToe.play(x, o);
        ticTacToe.play(x + 1, o + 1);
        char actual = ticTacToe.nextPlayer();

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenParametersWhenPlayThenNoWinner() {
        //given
        int x = 1;
        int o = 1;
        String expected = "No winner";

        //when
        String actual = ticTacToe.play(x, o);

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenParametersWhenPlayAndSetVerticalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        String actual = ticTacToe.play(3, 1); // X
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    void givenParametersWhenPlayAndSetHorizontalLineThenWinner() {
        //given
        String expected = "O is the winner";

        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); //O
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenParametersWhenPlayAndSetPrimaryDiagonalThenWin() {
        //given
        String expected = "X is the winner";

        //when
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        String actual = ticTacToe.play(3, 3); // X

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void givenParametersWhenPlayAndSetSecondaryDiagonalThenWin() {
        //given
        String expected = "X is the winner";

        //when
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 1); // O
        String actual = ticTacToe.play(3, 1); // X

        //then
        assertThat(actual).isEqualTo(expected);
    }


}
