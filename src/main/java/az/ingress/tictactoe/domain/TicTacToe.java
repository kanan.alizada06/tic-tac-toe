package az.ingress.tictactoe.domain;

/**
 * @author Kanan
 */
public class TicTacToe {

    private static final char EMPTY = '\0';
    private char[][] board = {{EMPTY, EMPTY, EMPTY},
            {EMPTY, EMPTY, EMPTY},
            {EMPTY, EMPTY, EMPTY}};

    private static final char playerX = 'X';
    private static final char playerO = 'O';
    private static final int SIZE = 3;
    private char turn = EMPTY;

    String play(int x, int o) {
        checkCoordinate(x, playerX);
        checkCoordinate(o, playerO);
        turn = nextPlayer();
        setBox(x, o, turn);
        if (isWin(x, o)) {
            return turn + " is the winner";
        } else {
            return "No winner";
        }
    }

    char nextPlayer() {
        if (turn == playerX) {
            return playerO;
        }
        return playerX;
    }

    private void checkCoordinate(int axis, char c) {
        if (axis < 1 || axis > SIZE) {
            throw new RuntimeException(c + " is outside of the board");
        }
    }

    private void setBox(int x, int o, char turn) {
        if (isOccupied(x, o)) throw new RuntimeException("Box is occupied");
        else board[x - 1][o - 1] = turn;
    }

    private boolean isOccupied(int x, int o) {
        return board[x - 1][o - 1] != EMPTY;
    }

    private boolean isWin(int x, int o) {
        int playerTotal = turn * SIZE;
        char horizontal, vertical, primary, secondary;
        horizontal = vertical = primary = secondary = EMPTY;
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][o - 1];
            vertical += board[x - 1][i];
            primary += board[i][i];
            secondary += board[i][SIZE - i - 1];
        }
        return horizontal == playerTotal
                || vertical == playerTotal
                || primary == playerTotal
                || secondary == playerTotal;
    }
}
